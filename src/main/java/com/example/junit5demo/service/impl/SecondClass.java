package com.example.junit5demo.service.impl;

import com.example.junit5demo.service.FirstIface;
import com.example.junit5demo.service.SecondIface;
import com.example.junit5demo.so.TestObj;

public class SecondClass implements SecondIface {

    public FirstIface firstIface;

    @Override
    public void callFirst() {
        firstIface.call();
    }

    @Override
    public Integer callNumber(Integer input) {
        System.out.println(input);
        return input + 10;
    }

    @Override
    public Integer printTestObj(TestObj testObj) {
        System.out.println(testObj);
        return 0;
    }
}
