package com.example.junit5demo;

import com.example.junit5demo.extensions.RandomParameterResolver;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.AfterAllCallback;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.extension.ExtensionContext;

import static com.example.junit5demo.extensions.RandomParameterResolver.Random;

@ExtendWith(RandomParameterResolver.class)
public class ExtendedTest implements AfterAllCallback {

    @Test
    void testRandoms(@Random int i, @Random long l, @Random double d) {
        System.out.println(i + " " + l + " " + d);
    }


    @Override
    public void afterAll(ExtensionContext extensionContext) throws Exception {
        System.out.println("This execute after all tests in class.");
    }
}
