package com.example.junit5demo.yannyandlaurel;

import org.springframework.stereotype.Service;

public interface WordProducer {

    String getWord();

}
