package com.example.junit5demo;

import org.junit.jupiter.api.*;

import java.util.HashMap;
import java.util.Map;

public class SpecialTest {

    @RepeatedTest(5)
    void repeatedTest() {
        System.out.println("test");
    }

    @RepeatedTest(value = 5, name = "{currentRepetition} opakovanie z {totalRepetitions}")
    void repeatedValuedTest() {
        System.out.println("test");
    }

    @RepeatedTest(10)
    void repeatedTestWithDI(TestInfo testInfo, RepetitionInfo repetitionInfo) {
        System.out.println(testInfo.getDisplayName());
        System.out.println(repetitionInfo.getCurrentRepetition() + " - " + repetitionInfo.getCurrentRepetition());
    }

    @Test
    void testReporter(TestReporter testReporter) {
        Map<String, String> values = new HashMap<>();
        values.put("user name", "dk38");
        values.put("award year", "1974");

        testReporter.publishEntry(values);
    }

    //kuknut TestReporter testReporter

}
