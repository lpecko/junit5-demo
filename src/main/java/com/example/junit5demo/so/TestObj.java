package com.example.junit5demo.so;

public class TestObj {

    private String s1;

    private Integer i1;

    public String getS1() {
        return s1;
    }

    public void setS1(String s1) {
        this.s1 = s1;
    }

    public Integer getI1() {
        return i1;
    }

    public void setI1(Integer i1) {
        this.i1 = i1;
    }

    @Override
    public String toString() {
        return "TestObj{" +
                "s1='" + s1 + '\'' +
                ", i1=" + i1 +
                '}';
    }
}
