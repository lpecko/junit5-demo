package com.example.junit5demo.yannyandlaurel;

import com.example.junit5demo.config.BasicConfiguration;
import com.example.junit5demo.config.LaurelConfiguration;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;

import static org.junit.jupiter.api.Assertions.*;

@SpringJUnitConfig
@ContextConfiguration(classes = {BasicConfiguration.class, LaurelConfiguration.class})
class LaurelHearingInterpreterIT {

    @Autowired
    private HearingInterpreter hearingInterpreter;

    @Test
    void whatIHeard() {
        String word = hearingInterpreter.whatIHeard();
        assertEquals("Laurel", word);
    }
}