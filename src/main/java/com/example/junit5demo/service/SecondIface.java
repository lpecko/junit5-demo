package com.example.junit5demo.service;

import com.example.junit5demo.so.TestObj;

public interface SecondIface {

    void callFirst();

    Integer callNumber(Integer input);

    Integer printTestObj(TestObj testObj);

}
