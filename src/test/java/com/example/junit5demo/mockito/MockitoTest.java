package com.example.junit5demo.mockito;


import com.example.junit5demo.service.impl.FirstClass;
import com.example.junit5demo.service.impl.SecondClass;
import com.example.junit5demo.so.TestObj;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class) // automaticke inicializovanie mockov
public class MockitoTest {

    @Mock
    private FirstClass first;

    @InjectMocks
    private SecondClass second;

    @Mock
    private SecondClass secondMocked;

    @Spy @InjectMocks
    private SecondClass secondSpyed;

//    @BeforeEach
//    void setUp() {
//        MockitoAnnotations.initMocks(first); // rucne inicializovanie
//    }


    @Test
    void testInjectedMocks() {
        second.callFirst(); // mockovane volanie
    }

    @Test
    void verifyTest() {
        secondMocked.callNumber(10);
        secondMocked.callNumber(20);
        secondMocked.callNumber(10);
        verify(secondMocked, times(2)).callNumber(10);
        verify(secondMocked, times(1)).callNumber(20);
        verify(secondMocked, atLeastOnce()).callNumber(10);
        verify(secondMocked, never()).callFirst();
        verify(secondMocked, times(3)).callNumber(anyInt());
        verifyZeroInteractions(first);
        verifyNoMoreInteractions(secondMocked);
    }

    @Test
    void testCallNumber() {
        when(secondMocked.callNumber(10)).thenReturn(100);
        Integer result = secondMocked.callNumber(10);
        assertEquals((Integer) 100, result);
        verify(secondMocked).callNumber(10);
    }

    @Test
    void testThrow() {
        when(secondMocked.callNumber(10)).thenThrow(new RuntimeException("boom"));
        assertThrows(RuntimeException.class, () -> secondMocked.callNumber(10));
    }

    @Test
    void testInnerArgument() {
        final String TST_MATCH = "test match";
        TestObj testObj = new TestObj();
        testObj.setS1(TST_MATCH);

        when(secondMocked.printTestObj(argThat((matcher) -> matcher.getS1().equals(TST_MATCH)))).thenReturn(0);
        Integer result = secondMocked.printTestObj(testObj);
        assertEquals((Integer) 0, result);
    }

    @Disabled
    @Test
    void testInnerArgumentNotMatches() {
        final String TST_MATCH = "test match";
        TestObj testObj = new TestObj();
        testObj.setS1("not match");

        when(secondMocked.printTestObj(argThat((matcher) -> matcher.getS1().equals(TST_MATCH)))).thenReturn(0);
        Integer result = secondMocked.printTestObj(testObj);
    }

    @Test
    void testInOrder() {
        doAnswer((a) -> {
                    first.call();
                    return null;
                }
        ).when(secondMocked).callFirst();

        secondMocked.callFirst();
        secondMocked.callNumber(10);
        secondMocked.callFirst();
        InOrder inOrder = inOrder(secondMocked, first);

        inOrder.verify(secondMocked).callFirst();
        inOrder.verify(first).call();
        inOrder.verify(secondMocked).callNumber(anyInt());
        inOrder.verify(secondMocked).callFirst();
        inOrder.verify(first).call();
    }

    @Disabled
    @Test
    void testInOrderFail() {
        doAnswer((a) -> {
                    first.call();
                    return null;
                }
        ).when(secondMocked).callFirst();

        secondMocked.callFirst();
        secondMocked.callNumber(10);
        secondMocked.callFirst();
        InOrder inOrder = inOrder(secondMocked, first);

        inOrder.verify(first).call();
        inOrder.verify(secondMocked).callFirst();
        inOrder.verify(secondMocked).callNumber(anyInt());
        inOrder.verify(secondMocked).callFirst();
        inOrder.verify(first).call();
    }

    @Test
    void testSpy() {
        doAnswer(a -> {
            System.out.println("Test spy!!!");
            return null;
        }).when(first).call();
        secondSpyed.callFirst(); // call real method
    }
}
