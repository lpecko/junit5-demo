package com.example.junit5demo.greeting;

import org.junit.jupiter.api.*;
import org.junit.jupiter.api.condition.EnabledIf;
import org.junit.jupiter.api.condition.EnabledOnOs;
import org.junit.jupiter.api.condition.OS;

import java.time.Duration;

import static org.junit.jupiter.api.Assertions.*;

@Tag("greetingTests")
class GreetingTest {

    Greeting greeting;

    @BeforeAll
    static void beforeAll() {
        System.out.println("Before all");
    }

    @BeforeEach
    void beforeEach() {
        greeting = new Greeting();
        System.out.println("Before each");
    }

    @AfterEach
    void afterEach() {
        System.out.println("After each");
    }

    @AfterAll
    static void afterAll() {
        System.out.println("After all");
    }


    @DisplayName("hello test")
    @Test
    void hello() {
        assertEquals("hello peter", greeting.hello("peter"), () -> "wrong greeting format");
        assertEquals("hello peter", greeting.hello("peter"), "wrong greeting format");
    }

    @Disabled
    @Test
    void helloDisabled() {
        assertEquals("hello peter", greeting.hello("peter"), () -> "wrong greeting format");
        assertEquals("hello peter", greeting.hello("peter"), "wrong greeting format");
    }

    @Test
    void group() {
        assertAll("Group of test",
                () -> assertEquals("hello", greeting.hello()),
                () -> assertEquals("hello peter", greeting.hello("peter"))
        );
    }

    @Disabled
    @Test
    void groupFailed() {
        assertAll("Group of test fail",
                () -> assertEquals("hell", greeting.hello()),
                () -> assertEquals("hell peter", greeting.hello("peter"))
        );
    }

    @Disabled
    @DisplayName("hello test failed")
    @Test
    void helloFailed() {
        assertEquals("hell peter", greeting.hello("peter"), () -> "wrong greeting format");
        assertEquals("hell peter", greeting.hello("peter"), "wrong greeting format");
    }


    @Test
    void helloException() {
        assertThrows(RuntimeException.class, () -> greeting.helloException());
        assertThrows(Exception.class, () -> greeting.helloException());
        assertDoesNotThrow(() -> greeting.hello());
    }

    @Disabled
    @Test
    void helloExceptionFailed() {
        assertThrows(NullPointerException.class, () -> greeting.helloException());
    }

    @Test
    void slowlyHello() {
        assertTimeout(Duration.ofSeconds(2), () -> greeting.slowlyHello("peter"));
    }

    @Disabled
    @Test
    void slowlyHelloFailed() {
        assertTimeout(Duration.ofMillis(200l), () -> greeting.slowlyHello("peter"));
    }

    @Disabled
    @Test
    void slowlyHelloTextFailed() {
        assertTimeout(Duration.ofSeconds(2), () -> {
            String text = greeting.slowlyHello("peter");
            assertEquals("hell peter", text);
        });
    }

    @Test
    void assumeTrue() {
        // neskonci chybou, skonci ako nevykonany test
        Assumptions.assumeTrue(false);
    }

    @Test
    @EnabledOnOs(OS.LINUX)
    void testOnLinux() {
        System.out.println("this is linux");
    }

    @Test
    @EnabledOnOs(OS.WINDOWS)
    void testOnWindows() {
        System.out.println("this is windows");
    }

    @Test
    @EnabledIf("new java.util.Random().nextBoolean()==true")
    void testOnCondition() {
        System.out.println("condition is true");
    }


}