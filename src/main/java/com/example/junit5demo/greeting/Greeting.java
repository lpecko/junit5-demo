package com.example.junit5demo.greeting;

public class Greeting {

    public String hello(String name) {
        return "hello " + name;
    }

    public String hello() {
        return "hello";
    }

    public String helloException() {
        throw new RuntimeException("Hello exception");
    }

    public String slowlyHello(String name) {
        try {
            Thread.sleep(1000l);
            return hello(name);
        } catch (InterruptedException e) {
        }
        return null;
    }

}
