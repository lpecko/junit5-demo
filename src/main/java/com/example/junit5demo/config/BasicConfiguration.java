package com.example.junit5demo.config;

import com.example.junit5demo.yannyandlaurel.HearingInterpreter;
import com.example.junit5demo.yannyandlaurel.WordProducer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BasicConfiguration {

    @Bean
    HearingInterpreter hearingInterpreter(WordProducer wordProducer) {
        return new HearingInterpreter(wordProducer);
    }

}
