package com.example.junit5demo;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

@DisplayName("Nested test -> ")
public class NestedTest {

    @Test
    @DisplayName("test")
    void test() {
        System.out.println("test");
    }

    @Nested
    @DisplayName("Inner test -> ")
    class InnerTests {

        @Test
        @DisplayName("inner1")
        void inner1() {
            System.out.println("inner1");
        }

        @Test
        @DisplayName("inner2")
        void inner2() {
            System.out.println("inner2");
        }

    }
}
