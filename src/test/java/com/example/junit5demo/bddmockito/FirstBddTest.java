package com.example.junit5demo.bddmockito;

import com.example.junit5demo.service.impl.SecondClass;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.BDDMockito.*;

@ExtendWith(MockitoExtension.class)
public class FirstBddTest {
    // Behavior Driven Development
    // BDD plugin v mockito -> iba iny zapis z rovnakou funkcnostou

    @Mock
    private SecondClass secondMocked;

//    @Test
//    void testCallNumber() {
//        //given
//        when(secondMocked.callNumber(10)).thenReturn(100);
//
//        //when
//        Integer result = secondMocked.callNumber(10);
//
//        //then
//        assertEquals((Integer) 100, result);
//        verify(secondMocked).callNumber(10);
//    }

    @Test
    void testBdd() {
        //given
        given(secondMocked.callNumber(10)).willReturn(100);

        //when
        Integer result = secondMocked.callNumber(10);

        //then
        assertEquals((Integer) 100, result);
        then(secondMocked).should().callNumber(10);
        then(secondMocked).should(times(1)).callNumber(10);
        then(secondMocked).should(atLeastOnce()).callNumber(10);
        then(secondMocked).should(never()).callFirst();
    }
}
