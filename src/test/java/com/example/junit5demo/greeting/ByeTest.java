package com.example.junit5demo.greeting;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

// mvn clean install -Dgroups=byeTests <- pouzitie
@Tag("byeTests")
public class ByeTest {

    @Test
    void goodBye() {
        System.out.println("GoodBye!");
    }
}
