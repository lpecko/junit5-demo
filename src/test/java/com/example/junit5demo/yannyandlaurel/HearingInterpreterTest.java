package com.example.junit5demo.yannyandlaurel;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class HearingInterpreterTest {

    private HearingInterpreter hearingInterpreter;

    @Test
    void whatIHeardLaurel() {
        hearingInterpreter = new HearingInterpreter(new LaurelWordProducer());
        String word = hearingInterpreter.whatIHeard();
        assertEquals("Laurel", word);
    }

    @Test
    void whatIHeardYanny() {
        hearingInterpreter = new HearingInterpreter(new YannyWordProducer());
        String word = hearingInterpreter.whatIHeard();
        assertEquals("Yanny", word);
    }
}