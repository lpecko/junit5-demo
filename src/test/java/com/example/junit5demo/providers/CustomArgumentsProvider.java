package com.example.junit5demo.providers;

import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.ArgumentsProvider;

import java.util.stream.Stream;

public class CustomArgumentsProvider implements ArgumentsProvider {
    @Override
    public Stream<? extends Arguments> provideArguments(ExtensionContext extensionContext) throws Exception {
        return Stream.of(
                Arguments.of("row1", 1, 8l),
                Arguments.of("row2", 2, 16l),
                Arguments.of("row3", 4, 32l),
                Arguments.of("row4", 8, 64l)
        );
    }
}
