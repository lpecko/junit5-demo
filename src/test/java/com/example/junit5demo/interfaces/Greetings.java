package com.example.junit5demo.interfaces;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.TestInstance;

@TestInstance(value = TestInstance.Lifecycle.PER_CLASS)
@Tag("GreetingsGroup")
public interface Greetings {

    @BeforeAll
    default void beforeAll() {
        System.out.println("Before all in class " + this.getClass().getSimpleName());
    }

    @BeforeEach
    default void beforeEach() {
        System.out.println("Before each");
    }

}
