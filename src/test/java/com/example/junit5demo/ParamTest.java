package com.example.junit5demo;

import com.example.junit5demo.providers.CustomArgumentsProvider;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.*;

import java.util.stream.Stream;

public class ParamTest {

    enum Tst {
        ONE, TWO
    }


    @ParameterizedTest(name = "String param test: {arguments}")
    @ValueSource(strings = {"test", "string", "param"})
    void stringParamTest(String param) {
        System.out.println(param);
    }

    @ParameterizedTest
    @EnumSource(Tst.class)
    void enumParamTest(Tst param) {
        System.out.println(param);
    }

    @DisplayName("CsvTest")
    @ParameterizedTest(name="{displayName}: row[{index}]")
    @CsvSource({
            "row1, 1, 2",
            "row2, 2, 4",
            "row3, 4, 8",
            "row4, 8, 16"
    })
//    @CsvFileSource()
    void enumParamTest(String param, int i, long l) {
        System.out.println(param + " " + i + " " + l);
    }

    @DisplayName("Method parametrized test")
    @ParameterizedTest
    @MethodSource("getArgs")
    void methodParamTest(String param, int i, long l) {
        System.out.println(param + " " + i + " " + l);
    }

    static Stream<Arguments> getArgs() {
        return Stream.of(
                Arguments.of("row1", 1, 2l),
                Arguments.of("row2", 2, 4l),
                Arguments.of("row3", 4, 8l),
                Arguments.of("row4", 8, 16l)
        );
    }

    @DisplayName("Method parametrized test")
    @ParameterizedTest
    @ArgumentsSource(CustomArgumentsProvider.class)
    void customArgumentParamTest(String param, int i, long l) {
        System.out.println(param + " " + i + " " + l);
    }



}
