package com.example.junit5demo.config;

import com.example.junit5demo.yannyandlaurel.YannyWordProducer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
public class YannyConfiguration {

    @Bean
    YannyWordProducer yannyWordProducer() {
        return new YannyWordProducer();
    }

}
