package com.example.junit5demo.config;

import com.example.junit5demo.yannyandlaurel.LaurelWordProducer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
public class LaurelConfiguration {

    @Bean
    LaurelWordProducer laurelWordProducer() {
        return new LaurelWordProducer();
    }

}
