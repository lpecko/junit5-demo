package com.example.junit5demo.service.impl;

import com.example.junit5demo.service.FirstIface;

public class FirstClass implements FirstIface {

    @Override
    public void call() {
        System.out.println("First class called!!!");
    }

}
